
**Playful Communication of Serious Research**
Spring 2020 - Syllabus 
Class Time: Thursdays 6:30pm - 9:25pm
Room: Room 407

Professor: Brett Peterson
Contact Email: bjp313@nyu.edu
Office Hours: Thursdays 5:30pm-6:30pm by appointment


**Overview**

Exhibition design is the art of marrying experience and information. The best do so seamlessly; the very best surprise and delight you along the way. 

In this class you will explore the craft of interactive exhibition design through practice. Working in small groups, you will select an NYU researcher whose work is of interest to you and create an interactive experience that presents this research to a broader, public audience. 

The course is roughly broken into 3 parts: The Conceptual phase, where we will define *what* the experience will be, the Schematic phase, where we will figure out *how* we'll accomplish it all, and finally, the Production phase where we will actually create our experiences.

Below is the schedule for the semester. Please note that the following dates and topics are subject to change.

| Date | Field Report | Topic | Assignment | Notes |
|------|--------------|-------|------------|-------|
|  Jan 30  |     | Discussion:<ul><li>Introduction to the class</li><li> Introductions to each other!</li><li> Research at NYU</li></ul> |  Find  interesting research topics and bring 2-3 ideas to class, written on individual index cards        |[Class notes](2020_playfulcommunication-coursesite/class1.pdf) |
|  Feb 6   |  Me!  |  Sharing: <ul><li>Discuss proposed research topics and form teams</li></ul> Discussion: <ul><li>Brainstorming do's and dont's</li></ul> Activity:<ul><li>Start brainstorming in teams</li></ul>    |    With your teams:<ul><li>Contact researchers </li><li>Have a first round of brainstorming around your topics</li><li>Bring brainstorming "artifacts" - sketches, mood boards, diagrams, or images that support your ideas. Please be prepared to present them in the next class.    | [Class notes](2020_playfulcommunication-coursesite/class2.pdf) | 
|  Feb 13   | Sid, Name   |   Sharing: <ul><li>Present brainstorming artifacts and ideas and give feedback to each other</li></ul> Disussion:<ul><li>The Conceptual Pacakge: Defining the Message, Audience and Form</li></ul>    |   Make sure to have met or a plan to meet with your researchers by this point to learn the information and discuss possible ideas! </br></br> Complete a first pass of your conceptual package including the following: <ul><li> Sketches, diagrams, existing imagery to help convey the form and feeling of your idea </li><li>A document describing the Message, Audience and Form of the project, including questions we'll pose in class.  |[Class notes](2020_playfulcommunication-coursesite/class3.pdf) <br><br> [Conceptual examples](2020_playfulcommunication-coursesite/conceptual/)| 
|  Feb 20  | Dawn, Zach  | Discussion:<ul><li>More considerations when crafting a proposal</li></ul> Continue working on Conceptual package        |   <ul><li>PDF of your completed conceptual package</li><li>Slide deck of conceptual package and a rehearsed presentation including everyone on the team to be presented **March 5th**       |[Class notes](2020_playfulcommunication-coursesite/class4.pdf) | 
|  Feb  27   | None |  No Class - Field Trip     |  Keep working on your presentations!        |
|  Mar 5     | Sydney, Chang |   Team presentations of Conceptual Packages!     |   Write a followup summary of feedback given during the presentations. Things to consider: <ul><li>How was the idea received? Will you change anything? Were any new ideas suggested that you'd like to incorporate? How did the act of presenting affect your ideas?       | |
|  Mar 12    | Tianxu, Sheng |   Discussion: <ul><li>Design Planning - The Schematic package:<ul><li>Interaction Design</li><li>Physical and digital interface design</li><li>Schedules</li><li>Cost</li>     |    Completed Schematic package detailing the following:<ul><li>User-flow diagrams</li><li>Systems diagrams</li><li>Drawings for fabrication</li><li>Schedule of work broken down for each team member</li><li>Cost summary</li>     |[Class notes](2020_playfulcommunication-coursesite/class6.pdf) | 
|  Mar 19    | None |   No Class - Enjoy the break!    |          | |
|  Mar 26    | Dan, Zoey  |   Sharing: <ul><li>Walk through your schematic packages</li></ul>Activity / Discussion:<ul><li>Prototyping!</li><li>User feedback</li>     | Design an activity where you can gather feedback on a part of your experience. Run user testing sessions and document the process. Be prepared to present on your findings.        | [Class notes](2020_playfulcommunication-coursesite/Class7_Corona.pdf)| 
|  Apr 2     | Monni, Katie |  Production workshop      |          | [Class notes](2020_playfulcommunication-coursesite/Class8_Corona.pdf)|
|  Apr 9     | Sylvan, Rebecca  |   Production workshop    |          | [Class notes](2020_playfulcommunication-coursesite/Class9_Corona.pdf)|
|  Apr 16     | None |   No Class   |          | |
|  Apr 23    | Lizzie, Nicholas | Production workshop    |          | |
|  Apr 30    | Young min, Henry |  Production workshop     |          | |
|  May 7     | None |  Final project presentations!       |          | |





**Sharing museum experiences**
Throughout the semester, you will be expected to visit an exhibit or installation of your choice and share your experience with the class. Some questions to answer when presenting to the class could be the following: 

- What was the message of the exhibit? 
- How did you feel?
- What was done well? What did you feel was lacking?
- Was media involved? How did it support the exbihit? Did it detract from the message? 

Each student will be assigned a week to present their findings and post them to their blog. Please have pictures and/or videos to support your brief presentation.


**AMNH Tour**
We will also have a behind-the-scenes tour of the Exhibitions department at the American Museum of Natural History scheduled around the middle of the semester. We'll see some of the work for upcoming shows and some interactive exhibits in their natural habitat.
